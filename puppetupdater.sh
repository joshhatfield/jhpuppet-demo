#!/bin/bash

# Used to maintain puppet on my test server

# Run with crontab */5 * * * * /root/puppetupdater.sh >/dev/null 2>&1

cd /root/jhpuppet-demo && git pull

cp -rv /root/jhpuppet-demo/environments /etc/puppetlabs/code/

cp -rv /root/jhpuppet-demo/hiera/* /etc/hieradata

rm -f /etc/puppetlabs/puppet/hiera.yaml && ln -s /etc/hieradata/hiera.yaml /etc/puppetlabs/puppet/hiera.yaml


