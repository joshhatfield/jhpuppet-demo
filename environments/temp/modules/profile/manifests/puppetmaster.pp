class profile::puppetmaster {
  class { 'puppetserver::repository': } ->
  class { 'puppetserver': }
  class { 'puppetdb': }
  class { 'puppetdb::master::config': }
  notify { "this is generated from role::puppetmaster": }
}