node 'dc1dapp01.example.com' {
  include role::puppetmaster
}



node 'dc1pweb01.example.com' {
  # More classes
  include role::webserver
  notify { "This is from the temp environment": }
  $value = hiera('testkey')
  notify { "Hieravalue: ${value}": }
}

node 'dc2pweb01.example.com' {
  # More classes
  include role::webserver
  notify { "This is from the temp environment": }
}