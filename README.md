# Puppet Demo

This is just my own personal puppet demo code for study. purposes.

uses my vagrant environment
https://gitlab.com/joshhatfield/jhvagrantenv

## Modules used

https://forge.puppet.com/puppetlabs/puppetdb

https://forge.puppet.com/camptocamp/puppetserver


### misc config

My /etc/puppetlabs//puppet/puppet.conf config
```
[master]
vardir = /opt/puppetlabs/server/data/puppetserver
logdir = /var/log/puppetlabs/puppetserver
rundir = /var/run/puppetlabs/puppetserver
pidfile = /var/run/puppetlabs/puppetserver/puppetserver.pid
codedir = /etc/puppetlabs/code
autosign = true
storeconfigs = true
storeconfigs_backend = puppetdb

[agent]
server = dc1dapp01.example.com
certname = dc1dapp01.example.com

```